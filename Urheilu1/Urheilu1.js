class Henkilo {
    constructor(etunimet, sukunimi, kutsumanimi, syntymavuosi) {
      this.etunimet = etunimet;
      this.sukunimi = sukunimi;
      this.kutsumanimi = kutsumanimi;
      this.syntymavuosi = syntymavuosi;
    }
}

class Urheilija extends Henkilo {
    constructor(etunimet, sukunimi, kutsumanimi, syntymavuosi,kuva, paino, laji,saavutukset) {
        super(etunimet, sukunimi, kutsumanimi, syntymavuosi); //kantaluokan kutsu
        this.kuva = kuva;
        this.paino = paino;
        this.laji = laji;
        this.saavutukset = saavutukset;
        console.log("Urheilija luotu", this);
    }

    setKuva(kuva) {
        this.kuva = kuva;
    }

    
    getKuva() {
        return this.kuva;
    }

    setPaino(paino) {
        this.paino = paino;
    }

    getPaino() {
        return this.paino;
    }

    setLaji(laji) {
        this.laji = laji;
    }

    getLaji() {
        return this.laji;
    }

    setSaavutukset(saavutukset) {
        this.saavutukset = saavutukset;
    }

    getSaavutukset() {
        return this.saavutukset;
    }
}
//esimerkit
var paavodate = new Date(1897-06-13);
var paavoNurmi = new Urheilija(["Paavo", "Johannes"], "Nurmi", "Paavo", paavodate, "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Paavo_Nurmi_%28Antwerp_1920%29.jpg/240px-Paavo_Nurmi_%28Antwerp_1920%29.jpg", 65, "juoksu", ["Antwerpen 1920, kulta, 10 000 m juoksu", "Pariisi 1924, kulta, 1 500 m juoksu", "Amsterdam 1928, kulta, 10 000 m juoksu"]);

var helenadate = new Date(1947-10-28);
var helenaTakalo = new Urheilija(["Anni", "Helena"], "Takalo", "Helena", helenadate, "https://alchetron.com/cdn/helena-takalo-160b54d7-f178-4e2c-81cf-5d69ed25e85-resize-750.jpeg", "hiihto", ["Lahti 1978, kulta, 5 km hiihto", "Sapporo 1972, hopea, 3x5 km hiihto", "Innsbruck 1976, kulta, 5 km hiihto"]);

console.log(paavoNurmi.getKuva());
console.log(helenaTakalo.getSaavutukset());